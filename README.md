## ASP.NET MVC 5

"Cinema" is a site where people can registered and booked tickets online. Admin can add films, genres, schedule and time when film goes. 
Site builded as ASP MVC Project with 3-layers architecture and some asp mvc features:
1. pattern "repository". Example: film repository (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.DAL/Repositories/FilmRepository.cs)
2. pattern "unit of work". (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.DAL/Repositories/CinameUnitOfWork.cs)
3. automapper. Example of automapper config (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Automapper/WebProfile.cs)
4. autofac. Exmaple of autofac config (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/DI/AutofacConfig.cs)
5. mvc filters. Example of Action Filter (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Filters/HistoryAttribute.cs)
6. mvc helpers. Example of Helper (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Helpers/DatePickerInput.cs)
7. mvc areas. Example of area (https://gitlab.com/SECONDSTAGE_ASP/Cinema/tree/master/Cinema.WEB/Areas/Admin)
8. ajax. Example. (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Scripts/main.js)
9. C# extensions. Example (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Extensions/CheckPosterExtension.cs)
10. partial views. Example (https://gitlab.com/SECONDSTAGE_ASP/Cinema/blob/master/Cinema.WEB/Views/Partial/_LoginPartial.cshtml)


You can add existing database that stored in Cinema.WEB/App_Data folder
Admin in this database:
admin@cinema.com
password: Qq12345678