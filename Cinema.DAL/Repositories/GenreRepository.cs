﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Repositories
{
    public class GenreRepository
    {
        private readonly CinemaContext _context;

        public GenreRepository(CinemaContext context)
        {
            _context = context;
        }

        public IEnumerable<Genre> GetAll()
        {
            return _context.Genres;
        }

        public Genre Get(string id)
        {
            return _context.Genres.Find(id);
        }

        public IEnumerable<Genre> GetInId(string[] ids)
        {
            return _context.Genres.Where(g => ids.Contains(g.Id));
        }

        public void Create(Genre item)
        {
            _context.Genres.Add(item);
        }

        public void Update(Genre item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(string id)
        {
            var genre = _context.Genres.Find(id);
            if (genre != null)
                _context.Genres.Remove(genre);
        }
    }
}
