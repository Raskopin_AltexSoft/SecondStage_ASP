﻿using System;
using System.Threading.Tasks;
using Cinema.DAL.Context;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.Repositories
{
    public class CinameUnitOfWork : ICinemaUnitOfWork
    {
        private readonly CinemaContext _context = new CinemaContext();
        private FilmRepository _filmRepository;
        private GenreRepository _genreRepository;
        private FileRepository _fileRepository;
        private SessionRepository _sessionRepository;
        private ScheduleRepository _scheduleRepository;
        private BookingRepository _bookingRepository;

        private bool _disposed = false;

        public async  Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public BookingRepository Bookings
        {
            get { return _bookingRepository ?? (_bookingRepository = new BookingRepository(_context)); }
        }

        public ScheduleRepository Schedules
        {
            get { return _scheduleRepository ?? (_scheduleRepository = new ScheduleRepository(_context)); }
        }
        public FilmRepository Films
        {
            get { return _filmRepository ?? (_filmRepository = new FilmRepository(_context)); }
        }

        public GenreRepository Genres
        {
            get { return _genreRepository ?? (_genreRepository = new GenreRepository(_context)); }
        }

        public FileRepository Files
        {
            get { return _fileRepository ?? (_fileRepository = new FileRepository(_context)); }
        }

        public SessionRepository Sessions
        {
            get { return _sessionRepository ?? (_sessionRepository = new SessionRepository(_context)); }
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
