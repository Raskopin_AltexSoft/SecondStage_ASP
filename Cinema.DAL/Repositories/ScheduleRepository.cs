﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.Repositories
{
    public class ScheduleRepository : IRepository<Schedule>
    {
        private readonly CinemaContext _context;

        public ScheduleRepository(CinemaContext context)
        {
            _context = context;
        }
        public IEnumerable<Schedule> GetAll()
        {
            return _context.Schedules.Include(s => s.Sessions);
        }

        public IEnumerable<Schedule> GetBeetwenDates(DateTime from, DateTime to)
        {
            return _context.Schedules.Where(s => s.From <= from && s.To >= to).Include(s => s.Sessions);
        }

        public Schedule Get(string id)
        {
            return _context.Schedules.Find(id);
        }

        public void Create(Schedule item)
        {
            _context.Schedules.Add(item);
        }

        public void Update(Schedule item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(string id)
        {
            var schedule = _context.Schedules.Find(id);
            if (schedule != null)
                _context.Schedules.Remove(schedule);
        }
    }
}