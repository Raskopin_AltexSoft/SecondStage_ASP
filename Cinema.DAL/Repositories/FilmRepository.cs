﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.Repositories
{
    public class FilmRepository : IRepository<Film>
    {
        private readonly CinemaContext _context;

        public FilmRepository(CinemaContext context)
        {
            _context = context;
        }

        public IEnumerable<Film> GetAll()
        {
            return _context.Films.Include(f => f.Sessions.Select(s => s.Schedule));
        }

        public IEnumerable<Film> GetAllBeetwenDates(DateTime start, DateTime final)
        {
            return _context.Films
                .Where(f => f.StartDate <= start && f.FinishDate >= final)
                .Include(f => f.Sessions.Select(s => s.Schedule));
        }
        public Film Get(string id)
        {
            return _context.Films.Find(id);
        }

        public void Create(Film item)
        {
            _context.Films.Add(item);
        }

        public void Update(Film item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(string id)
        {
            var film = _context.Films.Find(id);
            if (film != null)
                _context.Films.Remove(film);
        }
    }
}
