﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.Repositories
{
    public class BookingRepository : IRepository<Booking>
    {
        private readonly CinemaContext _context;

        public BookingRepository(CinemaContext context)
        {
            _context = context;
        }
        public IEnumerable<Booking> GetAll()
        {
            return _context.Bookings;
        }

        public Booking Get(string id)
        {
            return _context.Bookings.Find(id);
        }

        public void Create(Booking item)
        {
            _context.Bookings.Add(item);
        }

        public void Update(Booking item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(string id)
        {
            var booking = _context.Bookings.Find(id);
            if (booking != null)
                _context.Bookings.Remove(booking);
        }

        public bool IfSessionHasBookings(string sessionId)
        {
            return _context.Bookings.Any(b => b.SessionId == sessionId);
        }

        public bool IfBookingExists(int chairNumber, DateTime date, string sessionid)
        {
            return
                _context.Bookings.Any(
                    b => b.ChairNumber == chairNumber && EntityFunctions.TruncateTime(b.Date) == date.Date && b.SessionId == sessionid);
        }

        public List<int> GetChairsByDateAndSession(string sessionId, DateTime date)
        {
            return _context.Bookings.Where(b => b.SessionId == sessionId && EntityFunctions.TruncateTime(b.Date) == date.Date).Select(b => b.ChairNumber).ToList();
        }

        public List<Booking> GetBookingsBySessionAndDate(string sessionId, DateTime date)
        {
            return _context.Bookings.Where(b => b.SessionId == sessionId && EntityFunctions.TruncateTime(b.Date) == date.Date).ToList();
        }
    }
}