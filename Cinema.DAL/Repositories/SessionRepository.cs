﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.Repositories
{
    public class SessionRepository : IRepository<Session>
    {
        private readonly CinemaContext _context;

        public SessionRepository(CinemaContext context)
        {
            _context = context;
        }

        public IEnumerable<Session> GetAll()
        {
            return _context.Sessions.Include(s => s.Film);
        }

        public IEnumerable<Session> GetBySchedule(string scheduleId)
        {
            return _context.Sessions.Where(s => s.ScheduleId == scheduleId);
        }

        public Session Get(string id)
        {
            return _context.Sessions.Find(id);
        }

        public void Create(Session item)
        {
            _context.Sessions.Add(item);
        }

        public void Update(Session item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(string id)
        {
            var session = _context.Sessions.Find(id);
            if (session != null)
                _context.Sessions.Remove(session);
        }
    }
}