﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Cinema.DAL.Context;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Repositories
{
    public class HistoryRepository : IDisposable
    {
        private readonly HistoryContext _historyContext;

        public HistoryRepository(HistoryContext historyContext)
        {
            _historyContext = historyContext;
        }

        public IEnumerable<History> GetAll()
        {
            return _historyContext.History;
        }

        public History Get(string ip, string browser, DateTime dateTime)
        {
            return _historyContext.History.Find(ip, browser, dateTime.Date);
        }

        public void Create(History history)
        {
            _historyContext.History.Add(history);
        }

        public void Update(History history)
        {
            _historyContext.Entry(history).State = EntityState.Modified;
        }

        public void Save()
        {
            _historyContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _historyContext.Dispose();
        }
    }
}