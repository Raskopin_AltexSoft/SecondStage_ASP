﻿using System.Collections.Generic;
using System.Linq;
using Cinema.DAL.Context;
using Cinema.DAL.Enums;
using File = Cinema.DAL.Entities.File;

namespace Cinema.DAL.Repositories
{
    public class FileRepository
    {
        private readonly CinemaContext _context;
        public FileRepository(CinemaContext context)
        {
            _context = context;
        }

        public void Create(File item)
        {
            _context.Files.Add(item);
        }

        public void Delete(string id)
        {
            var file = _context.Files.Find(id);
            if (file != null)
            {
                _context.Files.Remove(file);
            }
                
        }

        public File Get(string id)
        {
            return _context.Files.Find(id);
        }

        public int GetCount(FileType type, string id)
        {
            return _context.Files.Where(f => f.Type == type).Count(f => f.Id == id);
        }

        public List<File> GetFilesByTypeAndId(FileType fileType, string id)
        {
            return _context.Files.Where(f => f.OwnerId == id && f.Type == fileType).ToList();
        }
    }
}
