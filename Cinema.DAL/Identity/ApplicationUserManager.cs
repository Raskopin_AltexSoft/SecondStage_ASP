﻿using Cinema.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace Cinema.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }
    }
}
