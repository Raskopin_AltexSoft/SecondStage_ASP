namespace Cinema.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cinema.DAL.Context.CinemaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Cinema.DAL.Context.CinemaContext";
        }

        protected override void Seed(Cinema.DAL.Context.CinemaContext context)
        {

        }
    }
}
