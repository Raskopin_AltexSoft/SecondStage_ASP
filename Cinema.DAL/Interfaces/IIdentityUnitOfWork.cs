﻿using System;
using System.Threading.Tasks;
using Cinema.DAL.Identity;

namespace Cinema.DAL.Interfaces
{
    public interface IIdentityUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get;  } 
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        void Save();
    }
}