﻿using System;
using System.Threading.Tasks;
using Cinema.DAL.Repositories;

namespace Cinema.DAL.Interfaces
{
    public interface ICinemaUnitOfWork : IDisposable
    {
        FilmRepository Films { get; }
        GenreRepository Genres { get; }
        FileRepository Files { get; }
        SessionRepository Sessions { get; }
        ScheduleRepository Schedules { get; }
        BookingRepository Bookings { get; }
        Task SaveAsync();
        void Save();
    }
}