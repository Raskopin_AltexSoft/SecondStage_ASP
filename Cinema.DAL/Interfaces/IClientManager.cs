﻿using System;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile item);
    }
}