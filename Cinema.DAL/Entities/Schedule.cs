﻿using System;
using System.Collections.Generic;

namespace Cinema.DAL.Entities
{
    public class Schedule
    {
        public Schedule()
        {
            Sessions = new List<Session>();
        }
        public string Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ICollection<Session> Sessions { get; set; } 
    }
}