﻿using System;

namespace Cinema.DAL.Entities
{
    public class Booking
    {
        public string Id { get; set; } 
        public string SessionId { get; set; }
        public Session Session { get; set; }
        public int ChairNumber { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
}