﻿using System;
using System.Collections.Generic;

namespace Cinema.DAL.Entities
{
    public class Session
    {
        public Session()
        {
            Bookings = new List<Booking>();
        }
        public string Id { get; set; }
        public DateTime Time { get; set;  }
        public string ScheduleId { get; set; }
        public Schedule Schedule { get; set;  }
        public string FilmId { get; set; }
        public Film Film { get; set; }
        public ICollection<Booking> Bookings { get; set; }
    }
}