﻿namespace Cinema.DAL.Entities
{
    public class Role
    {
        public string Id { set; get; }
        public string Name { set; get; }
    }
}