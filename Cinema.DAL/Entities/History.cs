﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
    public class History
    {
        [Column(Order = 0), Key]
        public string Ip { get; set; }
        [Column(Order = 1), Key]
        public string Browser { get; set; }
        [Column(Order = 2), Key]
        public DateTime Date { get; set; }
        public int Count { get; set; }
    }
}