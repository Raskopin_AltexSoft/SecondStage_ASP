﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Cinema.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ClientProfile ClientProfile { get; set; }
    }
}
