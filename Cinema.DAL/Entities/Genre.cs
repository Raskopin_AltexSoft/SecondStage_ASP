﻿using System;
using System.Collections.Generic;

namespace Cinema.DAL.Entities
{
    public class Genre : IEquatable<Genre>
    {
        public Genre()
        {
            Films = new List<Film>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Film> Films { get; set; }

        public bool Equals(Genre other)
        {
            return Id == other.Id && Name == other.Name;
        }

        public override int GetHashCode()
        {
            return (Id + Name).GetHashCode();
        }
    }
}
