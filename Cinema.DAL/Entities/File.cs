﻿using Cinema.DAL.Enums;

namespace Cinema.DAL.Entities
{
    public class File
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public FileType Type { get; set; }
        public string OwnerId { get; set; }
    }
}
