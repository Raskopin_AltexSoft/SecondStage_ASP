﻿using System;
using System.Collections.Generic;

namespace Cinema.DAL.Entities
{
    public class Film
    {
        public Film()
        {
            Genres = new List<Genre>();
            Sessions = new List<Session>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
        public virtual ICollection<Session> Sessions { get; set; } 
    }
}
