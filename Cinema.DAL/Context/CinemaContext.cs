﻿using System.Data.Entity;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Context
{
    public class CinemaContext : DbContext
    {
        public CinemaContext()
            : base(ConnectionString.DefaultConntecion)
        {
        }

        public DbSet<Film> Films { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>().HasMany(c => c.Films)
                .WithMany(s => s.Genres)
                .Map(t => t.MapLeftKey("GenreId")
                .MapRightKey("FilmId")
                .ToTable("GenreFilm"));
            modelBuilder.Entity<User>().ToTable("AspNetUsers");

        }
    }
}