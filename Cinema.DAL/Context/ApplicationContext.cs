﻿using System.Data.Entity;
using Cinema.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cinema.DAL.Context
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(string connectionString) : base(ConnectionString.DefaultConntecion)
        {
        }
        public DbSet<ClientProfile>  ClientProfiles { get; set; }
    }
}
