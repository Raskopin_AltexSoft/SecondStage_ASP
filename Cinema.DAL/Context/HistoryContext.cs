﻿using System.Data.Entity;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Context
{
    public class HistoryContext : DbContext
    {
        public HistoryContext()
            : base(ConnectionString.DefaultConntecion)
        {
            
        }
        public DbSet<History> History { get; set; }
    }
}
