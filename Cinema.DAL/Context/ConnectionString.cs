﻿using System.Configuration;
using System;
using System.Data;

namespace Cinema.DAL.Context
{
    public static class ConnectionString
    {
        public static string DefaultConntecion
        {
            get
            {

                // get from DAL app.config.
                var connectionString =
                    ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                return connectionString;
            }
        }
    }
}