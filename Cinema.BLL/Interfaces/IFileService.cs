﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cinema.BLL.DTO;
using Cinema.DAL.Enums;

namespace Cinema.BLL.Interfaces
{
    public interface IFileService : IDisposable
    {
        string Create(FileDto filmDto);
        int GetCount(FileType type, string id);
        List<string> GetFilesByTypeAndId(string type, string id);
    }
}
