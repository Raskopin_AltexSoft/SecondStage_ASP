﻿using System;
using System.Collections.Generic;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface IScheduleService : ICinemaService<ScheduleDto>, IDisposable
    {
        ScheduleDto GetByDate(DateTime date);
        List<ScheduleDto> GetByDateSchedules(DateTime date);
        List<ScheduleDto> GetBeetwenDates(DateTime from, DateTime to);
    }
}