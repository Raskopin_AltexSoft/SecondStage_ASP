﻿using System;
using System.Collections.Generic;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface IBookingService : ICinemaService<BookingDto>, IDisposable
    {
        List<BookingDto> GetBookingsBySessionAndDate(string sessionId, DateTime date);
        List<int> GetChairsByDateAndSession(string sessionId, DateTime date);
        bool IfSessionHasBookings(string sessionId);
        bool IfSeatBooking(int chairNumber, DateTime date, string sessionId);
    }
}