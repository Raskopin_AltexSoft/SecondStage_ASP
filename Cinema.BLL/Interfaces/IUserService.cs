﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        string Create(UserDto userDto);
        Task<ClaimsIdentity> Authenticate(UserDto userDto);
        Task SetInitialData(UserDto adminDto, List<string> roles);
    }
}