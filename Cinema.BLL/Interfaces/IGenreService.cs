﻿using System;
using System.Collections.Generic;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface IGenreService : ICinemaService<GenreDto>, IDisposable
    {
        IEnumerable<GenreDto> GetInId(string[] ids);
    }
}