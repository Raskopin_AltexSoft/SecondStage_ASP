﻿using System;
using System.Collections.Generic;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface ISessionService : ICinemaService<SessionDto>, IDisposable
    {
        List<SessionDto> GetSessionsBySchedule(string scheduleId);
        List<DateTime> GetFilmTimes(string filmName, DateTime date);
        string GetSessionIdByDateAndTime(DateTime date, DateTime time);
    }
}