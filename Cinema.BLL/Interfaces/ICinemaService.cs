﻿using System.Collections.Generic;

namespace Cinema.BLL.Interfaces
{
    public interface ICinemaService<T> where T : class
    {
        string Create(T t);
        string Edit(T t);
        string Delete(string id);
        IEnumerable<T> GetAll();
        T Get(string id);
    }
}
