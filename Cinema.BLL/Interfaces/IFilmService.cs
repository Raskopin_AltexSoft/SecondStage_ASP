﻿using System;
using System.Collections.Generic;
using Cinema.BLL.DTO;

namespace Cinema.BLL.Interfaces
{
    public interface IFilmService : ICinemaService<FilmDto>, IDisposable
    {
        List<FilmDto> GetCurrentFilms();
        IEnumerable<FilmDto> GetAllBeetwenDates(DateTime start, DateTime final);
    }
}
