﻿using AutoMapper;

namespace Cinema.BLL.Automapper
{
    public static class AutoMapperBll
    {
        private static readonly IMapper Mapper;

        static AutoMapperBll()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BllProfile>();
            });
            Mapper = config.CreateMapper();
        }

        public static IMapper GetMapper()
        {
            return Mapper;
        }
    }
}