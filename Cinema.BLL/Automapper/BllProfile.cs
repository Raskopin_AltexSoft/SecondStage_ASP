﻿using AutoMapper;
using Cinema.BLL.DTO;
using Cinema.DAL.Entities;

namespace Cinema.BLL.Automapper
{
    public class BllProfile : Profile
    {
        public BllProfile()
        {
            CreateMap<Film, FilmDto>();
            CreateMap<FilmDto, Film>();
            CreateMap<Genre, GenreDto>();
            CreateMap<GenreDto, Genre>();
            CreateMap<SessionDto, Session>();
            CreateMap<Session, SessionDto>()
                .ForMember(s => s.FilmDto,     opt => opt.MapFrom(src => src.Film)).MaxDepth(1)
                .ForMember(s => s.ScheduleDto, opt => opt.MapFrom(src => src.Schedule)).MaxDepth(1)
                .ForMember(s => s.BookingsDto, opt => opt.MapFrom(src => src.Bookings)).MaxDepth(1);
            CreateMap<ScheduleDto, Schedule>();
            CreateMap<Schedule, ScheduleDto>();
            CreateMap<BookingDto, Booking>()
                .ForMember(b => b.UserId, opt => opt.MapFrom(src => src.ApplicationUserId));
            CreateMap<Booking, BookingDto>();
        }
    }
}