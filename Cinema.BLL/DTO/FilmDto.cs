﻿using System;
using System.Collections.Generic;

namespace Cinema.BLL.DTO
{
    public class FilmDto
    {
        public FilmDto()
        {
            Genres = new List<GenreDto>();
            Sessions = new List<SessionDto>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public ICollection<GenreDto> Genres { get; set; }
        public ICollection<SessionDto> Sessions { get; set; } 
    }
}
