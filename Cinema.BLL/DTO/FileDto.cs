﻿namespace Cinema.BLL.DTO
{
    public class FileDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Type { get; set; }
        public string OwnerId { get; set; }
    }
}
