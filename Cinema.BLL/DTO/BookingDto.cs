﻿using System;
using Cinema.DAL.Entities;

namespace Cinema.BLL.DTO
{
    public class BookingDto
    {
        public string Id { get; set; }
        public string SessionId { get; set; }
        public SessionDto Session { get; set; }
        public int ChairNumber { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
}