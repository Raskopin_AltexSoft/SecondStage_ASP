﻿using System;
using System.Collections.Generic;

namespace Cinema.BLL.DTO
{
    public class SessionDto
    {
        public SessionDto()
        {
            BookingsDto = new List<BookingDto>();
        }
        public string Id { get; set; }
        public DateTime Time { get; set; }
        public string ScheduleId { get; set; }
        public ScheduleDto ScheduleDto { get; set; }
        public string FilmId { get; set; }
        public FilmDto FilmDto { get; set; }
        public ICollection<BookingDto> BookingsDto { get; set; }
    }
}