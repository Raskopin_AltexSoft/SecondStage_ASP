﻿using System;
using System.Collections.Generic;

namespace Cinema.BLL.DTO
{
    public class ScheduleDto
    {
        public ScheduleDto()
        {
            Sessions = new List<SessionDto>();
        }
        public string Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ICollection<SessionDto> Sessions { get; set; }
    }
}