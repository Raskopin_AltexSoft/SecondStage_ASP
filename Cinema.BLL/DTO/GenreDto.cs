﻿using System.ComponentModel.DataAnnotations;

namespace Cinema.BLL.DTO
{
    public class GenreDto
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
