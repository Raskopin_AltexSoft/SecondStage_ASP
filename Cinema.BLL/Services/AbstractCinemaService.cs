﻿using System;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public abstract class AbstractCinemaService : IDisposable
    {
        public AbstractCinemaService(ICinemaUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }
        protected ICinemaUnitOfWork Database { get; set; }
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
