﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;
using Microsoft.AspNet.Identity;

namespace Cinema.BLL.Services
{
    public class UserService : IUserService
    {
        private IIdentityUnitOfWork Database { get; set; }

        public UserService(IIdentityUnitOfWork uow)
        {
            Database = uow;
        }

        public string Create(UserDto userDto)
        {
            var user = Database.UserManager.FindByEmail(userDto.Email);
            if (user != null) return "User with given email already exists.";
            user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
            var result = Database.UserManager.Create(user, userDto.Password);
            if (result.Errors.Any())
                return string.Empty;
            Database.UserManager.AddToRole(user.Id, userDto.Role);
            var clientProfile = new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
            Database.ClientManager.Create(clientProfile);
            Database.Save();
            return "Successfull registration";
        }

        public async Task<ClaimsIdentity> Authenticate(UserDto userDto)
        {
            ClaimsIdentity claim = null;
            var user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        public async Task SetInitialData(UserDto adminDto, List<string> roles)
        {
            foreach (var roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role != null) continue;
                role = new ApplicationRole { Name = roleName };
                await Database.RoleManager.CreateAsync(role);
            }
            Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}