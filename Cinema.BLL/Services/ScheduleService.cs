﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class ScheduleService : AbstractCinemaService, IScheduleService
    {
        public ScheduleService(ICinemaUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public string Create(ScheduleDto scheduleDto)
        {
            var schedule = Database.Schedules.Get(scheduleDto.Id);
            if (schedule != null) return string.Empty;
            schedule = new Schedule
            {
                Id = scheduleDto.Id,
                From = scheduleDto.From,
                To = scheduleDto.To
            };
            Database.Schedules.Create(schedule);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public string Edit(ScheduleDto scheduleDto)
        {
            var schedule = Database.Schedules.Get(scheduleDto.Id);
            if (schedule == null) return "Schedule with given id does not exixt.";
            schedule.Sessions.Clear();
            schedule.From = scheduleDto.From;
            schedule.To = scheduleDto.To;
            foreach (var session in scheduleDto.Sessions)
            {
                schedule.Sessions.Add(AutoMapperBll.GetMapper().Map<Session>(session));
            }
            Database.Schedules.Update(schedule);
            return string.Empty;
        }

        public string Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<ScheduleDto> GetAll()
        {
            return AutoMapperBll.GetMapper().Map<List<ScheduleDto>>(Database.Schedules.GetAll());
        }

        public ScheduleDto Get(string id)
        {
            throw new System.NotImplementedException();
        }

        public ScheduleDto GetByDate(DateTime date)
        {
            var schedule = Database.Schedules.GetBeetwenDates(date, date);
            return AutoMapperBll.GetMapper().Map<ScheduleDto>(schedule);
        }

        public List<ScheduleDto> GetByDateSchedules(DateTime date)
        {
            var schedules = Database.Schedules.GetBeetwenDates(date, date); 
            return AutoMapperBll.GetMapper().Map<List<ScheduleDto>>(schedules);
        }

        public List<ScheduleDto> GetBeetwenDates(DateTime from, DateTime to)
        {
            var schedules = Database.Schedules.GetBeetwenDates(from, to);
            return AutoMapperBll.GetMapper().Map<List<ScheduleDto>>(schedules);
        }
    }
}