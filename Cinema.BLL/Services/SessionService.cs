﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class SessionService : AbstractCinemaService, ISessionService
    {
        private readonly IScheduleService _scheduleService;
        private readonly IFilmService _filmService;
        public SessionService(ICinemaUnitOfWork unitOfWork, IScheduleService scheduleService, IFilmService filmService)
            : base(unitOfWork)
        {
            _scheduleService = scheduleService;
            _filmService = filmService;
        }

        public string Create(SessionDto sessionDto)
        {
            var session = Database.Sessions.Get(sessionDto.Id);
            if (session != null) return string.Empty;
            session = new Session
            {
                Id = sessionDto.Id,
                ScheduleId = sessionDto.ScheduleId,
                FilmId = sessionDto.FilmId,
                Time = sessionDto.Time
            };
            Database.Sessions.Create(session);
            Database.Save();
            return string.Empty;
        }

        public string Edit(SessionDto t)
        {
            throw new System.NotImplementedException();
        }

        public string Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<SessionDto> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public SessionDto Get(string id)
        {
            throw new System.NotImplementedException();
        }

        public List<SessionDto> GetSessionsBySchedule(string scheduleId)
        {
            var sessions = Database.Sessions.GetBySchedule(scheduleId);
            return AutoMapperBll.GetMapper().Map<List<SessionDto>>(sessions);
        }
        public List<DateTime> GetFilmTimes(string filmName, DateTime date)
        {
            var schedulesDto = _scheduleService.GetByDateSchedules(date);
            var sessions = new List<SessionDto>();
            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));
            foreach (var session in sessions)
            {
                var film = _filmService.Get(session.FilmId);
                film.Sessions = null;
                session.FilmDto = film;
            }
            return sessions.Where(s => s.FilmDto.Name == filmName).OrderBy(s => s.Time).Select(s => s.Time).ToList();
        }

        public string GetSessionIdByDateAndTime(DateTime date, DateTime time)
        {
            var schedulesDto = _scheduleService.GetByDateSchedules(date);
            var sessions = new List<SessionDto>();
            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));
            return sessions.Where(s => s.Time.TimeOfDay == time.TimeOfDay).Select(s => s.Id).FirstOrDefault();
        }
    }
}