﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Enums;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class FilmService : AbstractCinemaService, IFilmService
    {
        public FilmService(ICinemaUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public string Create(FilmDto filmDto)
        {
            var film = Database.Films.Get(filmDto.Id);
            if (film != null) return ("Film with given id already exists");
            var genres = AutoMapperBll.GetMapper().Map<List<Genre>>(filmDto.Genres);
            film = new Film
            {
                Id = filmDto.Id,
                Duration = filmDto.Duration,
                Genres = new List<Genre>(),
                Name = filmDto.Name,
                StartDate = filmDto.StartDate,
                FinishDate = filmDto.FinishDate
            };
            foreach (var genre in genres)
            {
                film.Genres.Add(genre);
                Database.Genres.Update(genre);
            }
            Database.Films.Create(film);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public string Edit(FilmDto filmDto)
        {
            var film = Database.Films.Get(filmDto.Id);
            if (film == null) return "Film with given Id doesn't exist.";
            film.Duration = filmDto.Duration;
            film.Genres.Clear();
            var allGenres = Database.Genres.GetAll().ToList();
            foreach (var genre in allGenres)
            {
                if (filmDto.Genres.Select(g => g.Id).Contains(genre.Id))
                {
                    film.Genres.Add(genre);
                }
            }
            film.Name = filmDto.Name;
            film.StartDate = filmDto.StartDate;
            film.FinishDate = filmDto.FinishDate;
            Database.Films.Update(film);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public IEnumerable<FilmDto> GetAll()
        {
            var filmsDto = AutoMapperBll.GetMapper().Map<List<FilmDto>>(Database.Films.GetAll().ToList());
            foreach (var filmDto in filmsDto)
            {
                var genres = Database.Genres.GetAll().Where(g => g.Films.Any(f => f.Id == filmDto.Id));
                var genresDto = AutoMapperBll.GetMapper().Map<List<GenreDto>>(genres);
                filmDto.Genres = genresDto;
            }
            return filmsDto;
        }

        public FilmDto Get(string id)
        {
            return AutoMapperBll.GetMapper().Map<FilmDto>(Database.Films.Get(id));
        }

        public string Delete(string id)
        {
            var film = Database.Films.Get(id);
            if (film == null) return "Film with given Id does not exist.";
            Database.Films.Delete(id);
            var filesToDelete = Database.Files.GetFilesByTypeAndId(FileType.Poster, id);
            foreach (var fileToDelete in filesToDelete)
            {
                Database.Files.Delete(fileToDelete.Id);
            }
            Database.Save();
            return string.Empty;
        }

        public List<FilmDto> GetCurrentFilms()
        {
            var films = Database.Films.GetAllBeetwenDates(DateTime.Now, DateTime.Now);
            var filmsDto = AutoMapperBll.GetMapper().Map<List<FilmDto>>(films);
            return filmsDto
                .Where(f => f.Sessions
                    .Any(s => s.ScheduleDto.From < DateTime.Now && s.ScheduleDto.To > DateTime.Now))
                .ToList();
        }

        public IEnumerable<FilmDto> GetAllBeetwenDates(DateTime start, DateTime final)
        {
            var films = Database.Films.GetAllBeetwenDates(start, final);
            return AutoMapperBll.GetMapper().Map<List<FilmDto>>(films);
        }
    }
}
