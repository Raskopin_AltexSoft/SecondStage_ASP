﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Enums;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class FileService : AbstractCinemaService, IFileService
    {
        public FileService(ICinemaUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public string Create(FileDto fileDto)
        {
            var file = Database.Files.Get(fileDto.Id);
            FileType fileType;
            if (!Enum.TryParse(fileDto.Type, out fileType))
            {
                fileType = FileType.Undefined;
            }
            if (file != null)
                return "Such file already exists.";
            file = new File()
            {
                Id = fileDto.Id,
                Name = fileDto.Name,
                Type = fileType,
                Extension = fileDto.Extension,
                OwnerId = fileDto.OwnerId
            };
            Database.Files.Create(file);
            Database.Save();
            Database.Dispose();
            return string.Empty;
        }

        public int GetCount(FileType type, string id)
        {
            return Database.Files.GetCount(type, id);
        }

        public List<string> GetFilesByTypeAndId(string type, string id)
        {
            FileType fileType;
            FileType.TryParse(type, true, out fileType);
            var files = Database.Files.GetFilesByTypeAndId(fileType, id);;
            return files.Select(file => file.Name + file.Extension).ToList();
        }

        public string Delete(string id)
        {
            var file = Database.Files.Get(id);
            if (file != null)
            {
                Database.Files.Delete(id);
                Database.Save();
                return string.Empty;
            }
            return "File with given id does not exist.";
        }
    }
}
