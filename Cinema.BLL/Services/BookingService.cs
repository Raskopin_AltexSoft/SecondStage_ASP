﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class BookingService : AbstractCinemaService, IBookingService
    {
        public BookingService(ICinemaUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public string Create(BookingDto bookingDto)
        {
            var bookingFind = Database.Bookings.Get(bookingDto.Id);
            if (bookingFind != null) return ("Booking with given id already exists");
            var booking = AutoMapperBll.GetMapper().Map<Booking>(bookingDto);
            Database.Bookings.Create(booking);
            Database.Save();
            return string.Empty;
        }

        public string Edit(BookingDto bookingDto)
        {
            var bookingFind = Database.Bookings.Get(bookingDto.Id);
            if (bookingFind == null) return "Film with given Id doesn't exist.";
            bookingFind = AutoMapperBll.GetMapper().Map<Booking>(bookingDto);
            Database.Bookings.Update(bookingFind);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public IEnumerable<BookingDto> GetAll()
        {
            var bookingsDto = AutoMapperBll.GetMapper().Map<List<BookingDto>>(Database.Bookings.GetAll());
            return bookingsDto;
        }

        public BookingDto Get(string id)
        {
            return AutoMapperBll.GetMapper().Map<BookingDto>(Database.Bookings.Get(id));
        }

        public bool IfSeatBooking(int chairNumber, DateTime date, string sessionId)
        {
            return Database.Bookings.IfBookingExists(chairNumber, date, sessionId);
        }

        public string Delete(string id)
        {
            var booking = Database.Bookings.Get(id);
            if (booking == null) return "Booking with given Id does not exist.";
            Database.Bookings.Delete(id);
            Database.Save();
            return string.Empty;
        }

        public List<BookingDto> GetBookingsBySessionAndDate(string sessionId, DateTime date)
        {
            return AutoMapperBll.GetMapper().Map<List<BookingDto>>(Database.Bookings.GetBookingsBySessionAndDate(sessionId, date));
        }

        public bool IfSessionHasBookings(string sessionId)
        {
            return Database.Bookings.IfSessionHasBookings(sessionId);
        }

        public List<int> GetChairsByDateAndSession(string sessionId, DateTime date)
        {
            return Database.Bookings.GetChairsByDateAndSession(sessionId, date);
        }
    }
}