﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.BLL.Services
{
    public class GenreService : AbstractCinemaService, IGenreService
    {
        public GenreService(ICinemaUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public string Create(GenreDto genreDto)
        {
            var genre = Database.Genres.Get(genreDto.Id);
            if (genre != null) return "Genre with given id already exists";
            genre = new Genre()
            {
                Id = genreDto.Id,
                Name = genreDto.Name
            };
            Database.Genres.Create(genre);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public  string Edit(GenreDto genreDto)
        {
            var genre = Database.Genres.Get(genreDto.Id);
            if (genre == null) return "Genre with given Id does not exist.";
            genre.Name = genreDto.Name;
            Database.Genres.Update(genre);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public string Delete(string id)
        {
            var genre = Database.Genres.Get(id);
            if (genre == null) return "Genre with given Id does not exist";
            Database.Genres.Delete(id);
            Database.Save();
            Dispose();
            return string.Empty;
        }

        public IEnumerable<GenreDto> GetAll()
        {
            var genres = Database.Genres.GetAll().ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<Genre, GenreDto>());
            var genreDto = Mapper.Map<IEnumerable<Genre>, List<GenreDto>>(genres);
            Dispose();
            return genreDto;
        }

        public GenreDto Get(string id)
        {
            var genre = AutoMapperBll.GetMapper().Map <GenreDto>(Database.Genres.Get(id));
            Dispose();
            return genre;
        }

        public IEnumerable<GenreDto> GetInId(string[] ids)
        {
            var findedGenres = Database.Genres.GetInId(ids);
            Mapper.Initialize(cfg => cfg.CreateMap<Genre, GenreDto>());
            var genres =  Mapper.Map<IEnumerable<Genre>, IEnumerable<GenreDto>>(findedGenres);
            Dispose();
            return genres;
        }
    }
}

