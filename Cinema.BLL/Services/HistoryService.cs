﻿using System;
using Cinema.DAL.Entities;
using Cinema.DAL.Repositories;

namespace Cinema.BLL.Services
{
    public class HistoryService
    {
        private readonly HistoryRepository _historyRepository;
        public HistoryService(HistoryRepository historyRepository)
        {
            _historyRepository = historyRepository;
        }

        public void Create(string ip, string browser, DateTime date)
        {
            var historyNote = _historyRepository.Get(ip, browser, date);
            if (historyNote != null)
            {
                historyNote.Count++;
                _historyRepository.Update(historyNote);
            }
            else
            {
                historyNote = new History
                {
                    Browser = browser,
                    Count = 1,
                    Date = date,
                    Ip = ip
                };
                _historyRepository.Create(historyNote);
            }
            _historyRepository.Save();
        }
    }

}