﻿using Cinema.BLL.Interfaces;
using Cinema.DAL.Repositories;

namespace Cinema.BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new IdentityUnitOfWork(connection));
        }
    }
}