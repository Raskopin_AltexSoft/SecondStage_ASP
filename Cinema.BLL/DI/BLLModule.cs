﻿using Autofac;
using Cinema.BLL.Interfaces;
using Cinema.BLL.Services;
using Cinema.DAL.Context;
using Cinema.DAL.Interfaces;
using Cinema.DAL.Repositories;

namespace Cinema.BLL.DI
{
    public sealed class BllModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CinameUnitOfWork>().As<ICinemaUnitOfWork>();
            builder.RegisterType<FilmService>().As<IFilmService>();
            builder.RegisterType<FileService>().As<IFileService>();
            builder.RegisterType<GenreService>().As<IGenreService>();
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<ScheduleService>().As<IScheduleService>();
            builder.RegisterType<BookingService>().As<IBookingService>();
            builder.RegisterType<HistoryService>();
            builder.RegisterType<HistoryContext>();
            builder.RegisterType<HistoryRepository>();
        }
    }
}
