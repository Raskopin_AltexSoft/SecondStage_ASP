﻿
using System.Web.Mvc;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Cinema.BLL.DI;

namespace Cinema.WEB.DI
{
    public static class AutofacConfig
    {
        public static IContainer Container { get; private set; }
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            RegisterModules(builder);
            RegisterTypes(builder);
            Container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
        }

        private static void RegisterModules(ContainerBuilder builder)
        {
            builder.RegisterModule<BllModule>();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly).InstancePerHttpRequest();
        }
    }
}