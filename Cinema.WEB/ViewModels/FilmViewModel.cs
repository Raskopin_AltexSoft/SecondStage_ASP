﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cinema.BLL.DTO;
using Cinema.WEB.ViewModels;
using Foolproof;

namespace Cinema.WEB.ViewModels
{
    public class FilmViewModel
    {
        public FilmViewModel()
        {
            Genres = new List<GenreViewModel>();
        }
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public int Duration { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [GreaterThan("StartDate")]
        public DateTime FinishDate { get; set; }
        public ICollection<GenreViewModel> Genres { get; set; }
        public ICollection<string> Posters { get; set; }
        public ICollection<SessionDto> Sessions { get; set; }
    }
}