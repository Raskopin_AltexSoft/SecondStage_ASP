﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Foolproof;

namespace Cinema.WEB.ViewModels
{
    public class ScheduleViewModel
    {
        public ScheduleViewModel()
        {
            Sessions = new List<SessionViewModel>();
           // Films= new List<string>();
        }
        [Required]
        [DataType(DataType.Date)]
        public DateTime From { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [GreaterThan("From")]
        public DateTime To { get; set; }
        public List<SessionViewModel> Sessions { get; set; } 
    }
}