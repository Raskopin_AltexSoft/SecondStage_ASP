﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cinema.WEB.ViewModels
{
    public class GenreViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
      //  public virtual ICollection<FilmViewModel> Films { get; set; }
    }
}