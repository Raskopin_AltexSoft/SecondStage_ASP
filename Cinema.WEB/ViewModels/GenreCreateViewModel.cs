﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cinema.WEB.ViewModels
{
    public class GenreCreateViewModel
    {
        public GenreCreateViewModel()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
       // public virtual ICollection<FilmViewModel> Films { get; set; }
    }
}