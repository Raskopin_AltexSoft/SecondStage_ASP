﻿using System;
using System.Collections.Generic;

namespace Cinema.WEB.ViewModels
{
    public class ScheduleViewModelCreate
    {
        public ScheduleViewModelCreate()
        {
            TimeFilm = new Dictionary<DateTime, string>();
        }

        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Dictionary<DateTime, string> TimeFilm { get; set; }

    }
}