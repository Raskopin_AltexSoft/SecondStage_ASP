﻿using System;
using Cinema.BLL.DTO;

namespace Cinema.WEB.ViewModels
{
    public class SessionViewModel
    {
        public string Id { get; set; }
        public DateTime Time { get; set; }
        public string ScheduleId { get; set; }
        public ScheduleViewModel ScheduleViewModel { get; set; }
        public string FilmId { get; set; }
        public FilmViewModel FilmViewModel { get; set; }
    }
}
