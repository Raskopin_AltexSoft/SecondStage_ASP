﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Foolproof;

namespace Cinema.WEB.ViewModels
{
    public class ScheduleViewModelView
    {
        public ScheduleViewModelView()
        {
           Films = new List<string>();
           Times = new List<string>();
        }

        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public List<string> Films { get; set; }
        public List<string> Times { get; set; }
    }
}