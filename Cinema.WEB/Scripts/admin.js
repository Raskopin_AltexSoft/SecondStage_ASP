﻿$(document).ready(function () {

    $("#to").datepicker({
        autoClose: true,
        minDate: new Date(),
        onSelect: function () {
            getFullSchedule();
        }
    });

    $("#from").datepicker({
        autoClose: true,
        minDate: new Date(),
        onSelect: function (fd, d) {
            $("#to").datepicker().data("datepicker").selectDate(new Date(d));
            $("#to").datepicker().data("datepicker").update('minDate', new Date(d));
        }
    });

    $("#from").datepicker().data("datepicker").selectDate(new Date());

    function getFullSchedule() {
        var $from = $("#from").datepicker().data("datepicker").selectedDates[0];
        var $to = $("#to").datepicker().data("datepicker").selectedDates[0];
        var $parseFrom = parseDate($from);
        var $parseTo = parseDate($to);
        $.ajax({
            type: "POST",
            url: "/Admin/Schedule/JsonGetTimes",
            data: '{rawFromDate: "' + $parseFrom + '", rawToDate: "' + $parseTo + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $.each(response, function (index, element) {
                    var $day = $("<div class='day'>" + index + "</div>");
                    $("#timelaps").append($day);
                    $.each(element, function (index, element) {
                        alert($.datepicker.parseTime(index));
                        //alert()+ " - " + Date(element));
                    });
                });
            }
        });
    };

    function parseDate($date) {
        return ($date.getDate().toString().length === 1 ? "0"
            + $date.getDate() : $date.getDate())
            + "."
            + (($date.getMonth() + 1).toString().length === 1 ? "0"
            + ($date.getMonth() + 1) : ($date.getMonth() + 1))
            + "." + $date.getFullYear();
    }
});