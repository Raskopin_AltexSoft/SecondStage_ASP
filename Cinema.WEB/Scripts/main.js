﻿$(document).ready(function () {
    
    $("#link-about").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr("href"),
     	top = $(id).offset().top - 100;
        $("body,html").animate({ scrollTop: top }, 1500);
    });

    $("#link-contacts").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr("href"),
     	top = $(id).offset().top - 100;
        $("body,html").animate({ scrollTop: top }, 1500);
    });

    $("#datepicker1").datepicker({
        autoClose: true,
        minDate: new Date(),
        onSelect: function (fd, d) {
            if (!d) return;
            setSchedule(fd);
        }
    });

    $("#datepicker1").datepicker().data("datepicker").selectDate(new Date());


    $("#film-select").change(function (e) {
        filmChange(e.target.value);
    });
    

    $("#time-select").change(function () {
        setRoom();
    });

    $(document).ajaxError(function (e, xhr) {
        if (xhr.status === 403) {
            var response = $.parseJSON(xhr.responseText);
            window.location = response.LogOnUrl;
        }
    });

    function setSchedule($date) {
        var $existFilms = [];
        $.ajax({
            type: "POST",
            url: "/Home/JsonGetSchedule",
            data: '{rawDate: "' + $date + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.length > 0) {
                    $("#time-select").empty();
                    $("#film-select").empty();
                    for (var i = 0; i < response.length; i++) {
                        var parse = /Date\(([^)]+)\)/.exec(response[i].Time);
                        var dt = new Date(parseFloat(parse[1]));
                        $("#time-select").append($("<option></option")
                            .attr("value", parseTime(dt))
                            .text(parseTime(dt)));
                        if ($.inArray(response[i].FilmViewModel.Name, $existFilms) === -1) {
                            $("#film-select").append($("<option></option")
                            .attr("value", response[i].FilmViewModel.Name)
                            .text(response[i].FilmViewModel.Name));
                            $existFilms.push(response[i].FilmViewModel.Name);
                            if ($("#film-select").val() !== null) {
                                filmChange($("#film-select").val());
                            }
                        }
                    }
                    setRoom();
                }
            }
        });

    }

    function filmChange($film) {
        var $date = $("#datepicker1").datepicker().data("datepicker").selectedDates[0];
        var $parseDate = parseDate($date);
        $.ajax({
            type: "POST",
            url: "/Home/JsonSetTime",
            data: '{rawDate: "' + $parseDate + '", filmName: "' + $film +'"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.length > 0) {
                    $("#time-select").empty();
                    for (var i = 0; i < response.length; i++) {
                        var parse = /Date\(([^)]+)\)/.exec(response[i]);
                        var dt = new Date(parseFloat(parse[1]));
                        $("#time-select").append($("<option></option")
                            .attr("value", parseTime(dt))
                            .text(parseTime(dt)));
                    }
                    setRoom();
                }
            }
        });
    };

    function setRoom() {
        var $date = $("#datepicker1").datepicker().data("datepicker").selectedDates[0];
        var $film = $("#film-select").val();
        var $time = $("#time-select").val();
        var $parseDate = parseDate($date);
        $.ajax({
            type: "POST",
            url: "/Home/JsonSetRoom",
            data: '{rawDate: "' + $parseDate + '", rawTime: "' + $time + '", filmName: "' + $film + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                resetSeatsReserve();
                console.log(response.length);
                for (var i = 0; i < response.length; i++) {
                    
                    $("#" + response[i]).addClass("reserved");
                }
            }
        });
    };

    function resetSeatsReserve() {
        var $seats = $(".booked");
        $seats.each(function () {
            $(this).removeClass("booked");
        });
        $seats = $(".reserved");
        $seats.each(function () {
            $(this).removeClass("reserved");
        });
    }

    function parseDate($date) {
        return ($date.getDate().toString().length === 1 ? "0"
            + $date.getDate() : $date.getDate())
            + "."
            + (($date.getMonth() + 1).toString().length === 1 ? "0"
            + ($date.getMonth() + 1) : ($date.getMonth() + 1))
            + "." + $date.getFullYear();
    }

    function parseTime($date) {
        var $hours = $date.getHours();
        var $minutes = $date.getMinutes();
        return ($hours.toString().length === 1? 
            "0" + $hours : $hours)
            + ":"
            + ($minutes.toString().length === 1 ?
            "0" + $minutes : $minutes);
    }

    $(".seat").click(function(e) {
        e.preventDefault();
        var $seat = $("#" + e.target.id);
        if (!$seat.hasClass("reserved")) {
            if ($seat.hasClass("booked")) {
                $seat.removeClass("booked");
            } else {
                $seat.addClass("booked");
            }   
        }
    });

    $("#book-seats").click(function() {
        var $date = $("#datepicker1").datepicker().data("datepicker").selectedDates[0];
        var $parseDate = parseDate($date);
        var $film = $("#film-select").val();
        var $time = $("#time-select").val();
        var $seats = $(".booked");
        var $seatsId = [];
        $seats.each(function () {
            $seatsId.push($(this).attr("id"));
        });
        $.ajax({
            type: "POST",
            traditional: true,
            dataType: "json",
            url: "/Home/JsonBooking",
            data: '{rawDate: "' + $parseDate + '", rawTime: "' + $time + '", filmName: "' + $film +  '", seats: "' + $seatsId + '"}',
            contentType: "application/json; charset=utf-8",
            
            success: function () {
                setRoom();
                $("#result").show().delay(5000).fadeOut();
            }
        });

    });
});