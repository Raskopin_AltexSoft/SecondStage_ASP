﻿$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        autoplay: true,
        autoplaySpeed : 200,
        items: 1
    });


    $(".slick-carousel-about").slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false


    });
    
    var $dot = $(".owl-carousel .owl-dot");
    function goToPrevCarousel() {
        var $prev = $dot.filter(".active").prev();
        if ($prev.length == 0)
            $prev = $dot.last();
        $prev.trigger("click");
    }

    function goToNextCarousel() {
        var $next = $dot.filter(".active").next();
        if ($next.length == 0)
            $next = $dot.first();
        $next.trigger("click");
    };

    $(".film-left").click(function (e) {
        e.preventDefault();
        goToPrevCarousel();
    });

    $(".film-right").click(function (e) {
        e.preventDefault();
        goToNextCarousel();
    });
});