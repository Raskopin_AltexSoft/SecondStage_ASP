function getDaysInMonth(month,year) {
    return new Date(year, month+1, 0).getDate();
}

function getFirstDayOfWeek(month,year) {
    return new Date(year, month, 1).getDay();
}

function fullWeeksInMonth(month, year) {
    var count = 1;
    var lastInWeek = [1,7,6,5,4,3,2];
    var lastDays = parseInt(getDaysInMonth(month, year)) - lastInWeek[parseInt(getFirstDayOfWeek(month,year))];
    if (lastDays % 7 > 0)
        count++ ;
    count += Math.floor(lastDays / 7);
    return count;
}

function removeTBody() {
    var prevTbody = document.getElementById("datepicker-tbody");
    if (prevTbody != null)
    {
        prevTbody.parentNode.removeChild(prevTbody);
    }
}

function createMonth(month, year, currentDay ) {
  
    var currentDate = new Date();
    var isCurrentMonth = false;
    if (currentDate.getMonth() == month && currentDate.getFullYear() == year)
    {
        isCurrentMonth = true;
    }
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
    var prevTbody = document.getElementById("datepicker-tbody");
    var tbody = document.createElement("tbody");
    tbody.setAttribute("id", "datepicker-tbody");
    var table = document.getElementById("date-picker").getElementsByTagName("table")[0];
    var monthName = monthNames[new Date(year, month).getMonth()];
    var spanMonth = document.getElementById("current-month");
    var spanYear = document.getElementById("current-year");
    spanMonth.innerHTML = monthName;
    spanMonth.setAttribute("name", month);
    spanYear.innerHTML = year;
    spanYear.setAttribute("name", year);
    var monthDay = 1;
    var thDay = 1;
    table.appendChild(tbody);
    var weeks = fullWeeksInMonth(month, year);
    var daysInMonth = getDaysInMonth(month, year);

    for (var i = 1; i<= weeks; i++)
    {
        var th = document.createElement("tr");
        tbody.appendChild(th);
        for (var j = 1; j<=7; j++)
        {
            var td = document.createElement("td");

            var firstDayOfWeek = parseInt(getFirstDayOfWeek(month,year));
            if (firstDayOfWeek == 0 )
            {
                firstDayOfWeek = 7;
            }
            if (thDay >=  firstDayOfWeek && monthDay <= daysInMonth)
            {
                if ((isCurrentMonth && monthDay == currentDate.getDate() && currentDay == false) || (currentDay != false && monthDay == currentDay))
                    td.setAttribute("class", "currentDate");
                var dayLink = document.createElement("a");
                dayLink.setAttribute("class", "day");
                dayLink.setAttribute("href", "");
                dayLink.innerHTML = monthDay;
                td.appendChild(dayLink);
                monthDay++;
            }
            thDay++;
            th.appendChild(td);
        }
    }
    var days = document.getElementsByClassName("day");
    for (var i = 0; i < days.length; i++) {
        days[i].addEventListener('click', selectDay, false);
    }
}

function ready() {
    var datePickerContainer = document.getElementById("date-picker-container");
    
    var inputElement = datePickerContainer.previousSibling;
    if (inputElement != null && inputElement.getAttribute("value") != null) {
       
        var currentDate = new Date(inputElement.getAttribute("value"));
        var currentDay = currentDate.getDate();
    } else {
        var currentDate = new Date();
    }
    createMonth(currentDate.getMonth(), currentDate.getFullYear(), currentDay);
    var prevMonthLink = document.getElementById("prev-month");
    var nextMonthLink = document.getElementById("next-month");
    var currentMonth = document.getElementById("set-current");

    prevMonthLink.onclick = function (e) {
        e.preventDefault();
        var currentMonth = parseInt(document.getElementById("current-month").getAttribute("name"));
        var currentYear = parseInt(document.getElementById("current-year").getAttribute("name"));
        removeTBody();

        if (currentMonth === 0)
        {
            createMonth(11, currentYear-1, false);
        }
        else
        {
            createMonth(currentMonth-1, currentYear, false);
        }
    }
    nextMonthLink.onclick = function (e) {
        e.preventDefault();

        var currentMonth = parseInt(document.getElementById("current-month").getAttribute("name"));
        var currentYear = parseInt(document.getElementById("current-year").getAttribute("name"));
        removeTBody();
        if (currentMonth === 11) {
            
            createMonth(0, currentYear+1);
        }
        else
        {
            createMonth(currentMonth+1, currentYear);
        }
    }

    currentMonth.onclick = function (e) {
        e.preventDefault();
        removeTBody();
        var currentDate = new Date();
        createMonth(currentDate.getMonth(), currentDate.getFullYear(), false);
    }
}

function createDatePicker(e) {
    var datePicker = document.createElement("div");
    datePicker.setAttribute("id", "date-picker");
    var head = document.createElement("div");
    var headLinkPrev = document.createElement("a");
    headLinkPrev.setAttribute("id", "prev-month");
    headLinkPrev.setAttribute("href", "");
    headLinkPrev.innerHTML = "Prev";

    var headLinkNext = document.createElement("a");
    headLinkNext.setAttribute("id", "next-month");
    headLinkNext.setAttribute("href", "");
    headLinkNext.innerHTML = "Next";

    var headLinkCurrent = document.createElement("a");
    headLinkCurrent.setAttribute("id", "set-current");
    headLinkCurrent.setAttribute("href", "");
    headLinkCurrent.innerHTML = "&#9899;";

    var spanYear = document.createElement("span");
    spanYear.setAttribute("id", "current-year");
    spanYear.setAttribute("name", "");

    var spanMonth = document.createElement("span");
    spanMonth.setAttribute("id", "current-month");
    spanMonth.setAttribute("name", "");
    head.appendChild(headLinkPrev);
    head.appendChild(spanYear);
    head.appendChild(headLinkCurrent);
    head.appendChild(spanMonth);
    head.appendChild(headLinkNext);

    datePicker.appendChild(head);

    var table = document.createElement("table");
    table.setAttribute("class", "table");
    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    var th1 = document.createElement("th");
    th1.setAttribute("class", "week-day");
    th1.innerHTML = "Mo";
    var th2 = document.createElement("th");
    th2.setAttribute("class", "week-day");
    th2.innerHTML = "Tu";
    var th3 = document.createElement("th");
    th3.setAttribute("class", "week-day");
    th3.innerHTML = "We";
    var th4 = document.createElement("th");
    th4.setAttribute("class", "week-day");
    th4.innerHTML = "Th";
    var th5 = document.createElement("th");
    th5.setAttribute("class", "week-day");
    th5.innerHTML = "Fr";
    var th6 = document.createElement("th");
    th6.setAttribute("class", "week-day");
    th6.innerHTML = "Sa";
    var th7 = document.createElement("th");
    th7.setAttribute("class", "week-day");
    th7.innerHTML = "Su";
    tr.appendChild(th1);
    tr.appendChild(th2);
    tr.appendChild(th3);
    tr.appendChild(th4);
    tr.appendChild(th5);
    tr.appendChild(th6);
    tr.appendChild(th7);
    thead.appendChild(tr);
    table.appendChild(thead);
    datePicker.appendChild(table);

    var datePickerInputs = document.getElementsByClassName("datepicker");

    var afterDiv = document.createElement("div");
    afterDiv.setAttribute("id", "date-picker-container");
    insertAfter(afterDiv, e.target);
    afterDiv.appendChild(datePicker);
    ready();
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function destroyDatePicker() {
        var datePicker = document.getElementById("date-picker");
        var datePickerContainer = document.getElementById("date-picker-container");
        datePicker.parentNode.removeChild(datePicker);
        datePickerContainer.parentNode.removeChild(datePickerContainer);
}

selectDay = function(e) {
    e.preventDefault();
    var targetInput = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("datepicker")[0];
    var attribute = this.innerHTML;
    //targetInput.setAttribute("value", document.getElementById("current-year").innerHTML + "-" + (parseInt(document.getElementById("current-month").getAttribute("name")) + 1) + "-" + attribute);
    var currentMonth = (parseInt(document.getElementById("current-month").getAttribute("name")) + 1);
    if (currentMonth < 10) { currentMonth = '0' + currentMonth; }
    if (attribute < 10) { attribute = '0' + attribute; }
    var dateString = document.getElementById("current-year").innerHTML + "-" + currentMonth + "-" + attribute;
    var date = new Date(dateString);
    //var d = date.format("dd.mm.yyyy");
    targetInput.setAttribute("value", dateString);

    destroyDatePicker();

};

document.addEventListener("DOMContentLoaded", init);
function init() {
    var dps = document.getElementsByClassName("datepicker");
    for (var i = 0; i < dps.length; i++)
    {
        dps[i].addEventListener("click", function(e) {
            createDatePicker(e);
        });
    }
}

