﻿using System.Collections.Generic;
using System.Web.Mvc;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.WEB.Automapper;
using Cinema.WEB.ViewModels;

namespace Cinema.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class GenresController : Controller
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        public ActionResult Index()
        {
            var genresDto = _genreService.GetAll();
            var genres = AutoMapperWeb.GetMapper().Map<List<GenreViewModel>>(genresDto);
            return View(genres);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var genreDto = _genreService.Get(id);
            var genreViewModel = AutoMapperWeb.GetMapper().Map < GenreViewModel>(genreDto);
            return View(genreViewModel);
        }

        [HttpPost]
        public ActionResult Edit(GenreViewModel genre)
        {
            _genreService.Edit(AutoMapperWeb.GetMapper().Map<GenreDto>(genre));
            return RedirectToAction("Index");
        }

        public ActionResult Delete(string id)
        {
            _genreService.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(GenreCreateViewModel genre)
        {
            if (!ModelState.IsValid) return View();
            var genreDto = AutoMapperWeb.GetMapper().Map<GenreDto>(genre);
            _genreService.Create(genreDto);
                
            return RedirectToAction("Index");
        }
    }
}