﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.WEB.Automapper;
using Cinema.WEB.ViewModels;

namespace Cinema.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ScheduleController : Controller
    {
        private readonly IFilmService _filmService;
        private readonly IScheduleService _scheduleService;
        private readonly ISessionService _sessionService;

        public ScheduleController(IFilmService filmService, IScheduleService scheduleService, ISessionService sessionService)
        {
            _filmService = filmService;
            _scheduleService = scheduleService;
            _sessionService = sessionService;
        }

        [HttpGet]
        public ActionResult Index(DateTime? date)
        {
            if (date == null)
                date = DateTime.Now;
            var schedulesDto = _scheduleService.GetByDateSchedules((DateTime)date);
            var sessions = new List<SessionDto>();
            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));

            sessions = sessions.OrderBy(s => s.Time.TimeOfDay).ToList();

            var sessionsViewModel = AutoMapperWeb.GetMapper().Map<List<SessionViewModel>>(sessions);
            foreach (var session in sessionsViewModel)
            {
                var film = _filmService.Get(session.FilmId);
                film.Sessions = null;
                session.FilmViewModel = AutoMapperWeb.GetMapper().Map<FilmViewModel>(film);
            }

            return View(sessionsViewModel);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var currentFilmsDto = _filmService.GetAllBeetwenDates(DateTime.Now, DateTime.Now).ToList();
            var currentFilmsViewModel = AutoMapperWeb.GetMapper().Map<List<FilmViewModel>>(currentFilmsDto);
            ViewBag.currentFilms = currentFilmsViewModel;
            return View();
        }

        [HttpPost]
        public ActionResult Add(ScheduleViewModelCreate schedule)
        {
            if (!ModelState.IsValid) return View();
            var times = Request.Form.GetValues("time[]");
            var films = Request.Form.GetValues("film[]");

            if (times == null || films == null) return RedirectToAction("Index");
            var i = 0;
            foreach (var time in times)
            {
                if (time != "" && films[i] != "")
                {
                    DateTime parseTime;
                    if (DateTime.TryParseExact(time, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                    {
                        schedule.TimeFilm.Add(parseTime, films[i]);
                    }
                }
                i++;
            }
            var scheduleDto = new ScheduleDto
            {
                Id = Guid.NewGuid().ToString(),
                From = schedule.From,
                To = schedule.To
            };
            _scheduleService.Create(scheduleDto);

            foreach (var timeFilm in schedule.TimeFilm)
            {
                var session = new SessionDto
                {
                    Id = Guid.NewGuid().ToString(),
                    FilmId = timeFilm.Value,
                    Time = timeFilm.Key,
                    ScheduleId = scheduleDto.Id
                };
                _sessionService.Create(session);
            }
            return RedirectToAction("Index");
        }

        public JsonResult JsonGetTimes(string rawFromdate, string rawToDate)
        {
            DateTime dateFrom;
            DateTime dateTo;
            if (rawFromdate == null || rawToDate == null)
                return Json("not all items have got", JsonRequestBehavior.AllowGet);
            if (!DateTime.TryParse(rawFromdate, out dateFrom) || !DateTime.TryParse(rawToDate, out dateTo))
            {
                return Json("One of dates is wrong.", JsonRequestBehavior.AllowGet);
            }

            var schedulesDto = _scheduleService.GetBeetwenDates(dateFrom, dateTo);
            var sessions = new List<SessionDto>();

            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));
            sessions = sessions.OrderBy(s => s.Time.TimeOfDay).ToList();

            foreach (var session in sessions)
            {
                var film = _filmService.Get(session.FilmId);
                film.Sessions = null;
                session.FilmDto = film;
            }

            var timeLine = new Dictionary<string, Dictionary<string, string>>();

            for (var dt = dateFrom; dt <= dateTo; dt = dt.AddDays(1))
            {
                var times = sessions
                    .Where(s => s.ScheduleDto.From <= dt && s.ScheduleDto.To >= dt)
                    .ToDictionary(
                        session => session.Time.ToShortTimeString(), 
                        session => session.Time.AddMinutes(session.FilmDto.Duration).ToShortTimeString());
                timeLine.Add(dt.ToShortDateString(), times);
            }
            return Json(timeLine, JsonRequestBehavior.AllowGet);
        }
    }
}