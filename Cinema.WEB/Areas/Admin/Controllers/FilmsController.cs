﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.WEB.Automapper;
using Cinema.WEB.Extensions;
using Cinema.WEB.ViewModels;

namespace Cinema.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class FilmsController : Controller
    {
        private readonly IFilmService _filmService;
        private readonly IFileService _fileService;
        private readonly IGenreService _genreService;

        public FilmsController(IFilmService filmService, IFileService fileService, IGenreService genreService)
        {
            _filmService = filmService;
            _fileService = fileService;
            _genreService = genreService;
        }

        public ActionResult Index()
        {
            var filmsDto = _filmService.GetAll().ToList();
            var filmsViewModel = AutoMapperWeb.GetMapper().Map<List<FilmViewModel>>(filmsDto);
            foreach (var filmViewModel in filmsViewModel)
            {
                filmViewModel.Posters = new List<string>(_fileService.GetFilesByTypeAndId("poster", filmViewModel.Id));
            }
            return View(filmsViewModel);
        }

        public ActionResult Delete(string id)
        {
            var filesToDelete = _fileService.GetFilesByTypeAndId("Poster", id);
            var targetFolder = HttpContext.Server.MapPath("~/Content/Files/Images/Posters");
            foreach (var fileToDelete in filesToDelete)
            {
                var pathToFile = Path.Combine(targetFolder, fileToDelete);
                System.IO.File.Delete(pathToFile);
            }
            _filmService.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            ViewBag.Genres = AutoMapperWeb.GetMapper().Map<List<GenreDto>>(_genreService.GetAll());
            var flimViewModel = AutoMapperWeb.GetMapper().Map<FilmViewModel>(_filmService.Get(id));
            return View(flimViewModel);
        }

        [HttpPost]
        public ActionResult Edit(FilmViewModel film, string[] selectedGenres)
        {
            if (!ModelState.IsValid) return View();
            var filmDto = AutoMapperWeb.GetMapper().Map<FilmDto>(film);
            if (selectedGenres != null)
            {
                foreach (var genre in _genreService.GetInId(selectedGenres))
                {
                    filmDto.Genres.Add(genre);
                }
            }
            _filmService.Edit(filmDto);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Genres = _genreService.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Add(FilmViewModel film, HttpPostedFileBase uploadImage, string[] selectedGenres)
        {
            var validatingImageResult = uploadImage.CheckPoster();

            if (ModelState.IsValid  && validatingImageResult == null)
            {
                film.Id = Guid.NewGuid().ToString();

                var image = Image.FromStream(uploadImage.InputStream, true, true);
                var extension = uploadImage.FileName.Substring(uploadImage.FileName.LastIndexOf('.'));
                var targetFolder = HttpContext.Server.MapPath("~/Content/Files/Images/Posters");
                var posterName = film.Name.Trim().Replace(' ', '_').Replace(':','_').ToLower() + "_1";
                var targetPath = Path.Combine(targetFolder,  posterName + extension);

                image.Save(targetPath);

                var posterId = Guid.NewGuid().ToString();
                var poster = new FileDto()
                {
                    Id = posterId,
                    Extension = extension,
                    Name = posterName,
                    Type = "Poster",
                    OwnerId = film.Id
                };
                _fileService.Create(poster);
                var filmDto = AutoMapperWeb.GetMapper().Map<FilmDto>(film);

                if (selectedGenres != null)
                {
                    foreach (var genre in _genreService.GetInId(selectedGenres))
                    {
                        filmDto.Genres.Add(genre);
                    }
                }
                _filmService.Create(filmDto);
                return RedirectToAction("Index");
            }
            @ViewBag.ImageErrorMessage = validatingImageResult;
            return View();
        }
    }
}