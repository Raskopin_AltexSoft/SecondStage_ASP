﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Cinema.WEB.Automapper;
using Cinema.WEB.DI;

namespace Cinema.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // AutoMapperWeb.Configure();
            //Class1 c1 = new Class1();
           // c1.Run();
        }

    }
}
