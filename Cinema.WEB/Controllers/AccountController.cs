﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.WEB.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Cinema.WEB.Controllers
{
    public class AccountController : Controller
    {
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            await SetInitialDataAsync();
            if (!ModelState.IsValid) return View(model);
            var userDto = new UserDto { Email = model.Email, Password = model.Password };
            var claim = await UserService.Authenticate(userDto);
            if (claim == null)
            {
                ModelState.AddModelError("", "Неверный логин или пароль.");
            }
            else
            {
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            await SetInitialDataAsync();
            if (!ModelState.IsValid) return View(model);
            var userDto = new UserDto
            {
                Email = model.Email,
                Password = model.Password,
                Address = model.Address,
                Name = model.Name,
                Role = "user"
            };
            UserService.Create(userDto);
            return RedirectToAction("Index", "Home");
        }

        private async Task SetInitialDataAsync()
        {
            await UserService.SetInitialData(new UserDto
            {
                Email = "admin@cinema.com",
                UserName = "admin@cinema.com",
                Password = "Qq12345678",
                Name = "Jhon Smith",
                Address = "Broadway",
                Role = "admin",
            }, new List<string> { "user", "admin" });
        }
    }
}