﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.WEB.Automapper;
using Cinema.WEB.Filters;
using Cinema.WEB.ViewModels;
using Microsoft.AspNet.Identity;

namespace Cinema.WEB.Controllers
{
    [History]
    public class HomeController : Controller
    {
        private readonly IFilmService _filmService;
        private readonly IFileService _fileService;
        private readonly IScheduleService _scheduleService;
        private readonly IBookingService _bookingService;
        private readonly ISessionService _sessionService;

        public HomeController(
            IFilmService filmService, 
            IFileService fileService, 
            IScheduleService scheduleService, 
            IBookingService bookingService,
            ISessionService sessionService)
        {
            _filmService = filmService;
            _fileService = fileService;
            _scheduleService = scheduleService;
            _bookingService = bookingService;
            _sessionService = sessionService;
        }
        public ActionResult Index()
        {

            var currentFilmsDto = _filmService.GetCurrentFilms();

            var currentFilmsViewModel = AutoMapperWeb.GetMapper().Map<List<FilmViewModel>>(currentFilmsDto);
            foreach (var currentFilmViewModel in currentFilmsViewModel)
            {
                currentFilmViewModel.Posters = new List<string>(_fileService.GetFilesByTypeAndId("poster", currentFilmViewModel.Id));
            }
            return View(currentFilmsViewModel);
        }

        public JsonResult JsonGetSchedule(string rawDate)
        {
            var date = DateTime.Now;
            if (rawDate != null)
            {
                DateTime.TryParse(rawDate, out date);
            }
            var schedulesDto = _scheduleService.GetByDateSchedules(date);
            var sessions = new List<SessionDto>();

            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));
            sessions = sessions.OrderBy(s => s.Time.TimeOfDay).ToList();

            var sessionsViewModel = AutoMapperWeb.GetMapper().Map<List<SessionViewModel>>(sessions);
            SetFilmsToSessions(sessionsViewModel);

            return Json(sessionsViewModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonSetTime(string rawDate, string filmName)
        {
            var date = DateTime.Now;
            if (rawDate != null)
            {
                DateTime.TryParse(rawDate, out date);
            }
            return Json(_sessionService.GetFilmTimes(filmName, date), JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonSetRoom(string rawDate, string rawTime, string filmName)
        {
            DateTime date;
            if (rawDate == null || rawTime == null || filmName == null)
                return Json("not all items choosed", JsonRequestBehavior.AllowGet);

            if (!DateTime.TryParse(rawDate, out date))
            {
                return Json("not date", JsonRequestBehavior.AllowGet);
            }
            DateTime time;
            if (!DateTime.TryParseExact(rawTime, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out time))
                return Json("Wrong time format.", JsonRequestBehavior.AllowGet);

            var sessionId = _sessionService.GetSessionIdByDateAndTime(date, time);

            if (sessionId == null)
                return Json("Such schedule item is not exists.", JsonRequestBehavior.AllowGet);

            var seats = _bookingService.GetChairsByDateAndSession(sessionId, date);
            return Json(seats, JsonRequestBehavior.AllowGet);
        }

        [AjaxAuthorize]
        public JsonResult JsonBooking(string rawDate, string rawTime, string filmName, string seats)
        {
            DateTime date;
            if (rawDate == null || rawTime == null || filmName == null || seats == null)
                return Json("not all items choosed", JsonRequestBehavior.AllowGet);
            if (!DateTime.TryParse(rawDate, out date))
            {
                return Json("not date", JsonRequestBehavior.AllowGet);
            }
            DateTime time;

            if (!DateTime.TryParseExact(rawTime, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out time))
                return Json("Wrong time format.", JsonRequestBehavior.AllowGet);

            var schedulesDto = _scheduleService.GetByDateSchedules(date);
            var sessions = new List<SessionDto>();

            schedulesDto.ForEach(s => sessions.AddRange(s.Sessions));
            var session = sessions.First(s => s.Time.TimeOfDay == time.TimeOfDay);

            if (session == null)
                return Json("Such schedule item is exists.", JsonRequestBehavior.AllowGet);

            var bookedChairs = new StringBuilder();
            var bookedChairsInt = new List<int>();
            var parseSeats = seats.Split(',').Select(s => s.ToString()).ToArray();

            foreach (var seat in parseSeats)
            {
                int chairNumber;
                if (!int.TryParse(seat, out chairNumber))
                {
                    return Json("Wrong chair number", JsonRequestBehavior.AllowGet);
                }
                if (_bookingService.IfSeatBooking(chairNumber, date, session.Id))
                {
                    if (bookedChairs.Length == 0)
                        bookedChairs.Append(chairNumber);
                    else
                        bookedChairs.Append(", " + chairNumber);
                }
                bookedChairsInt.Add(chairNumber);
            }

            if (bookedChairs.Length > 0)
                return Json("Chairs " + bookedChairs.ToString() + " are already booked.", JsonRequestBehavior.AllowGet);

            var userId = User.Identity.GetUserId();
            foreach (var bookedChair in bookedChairsInt)
            {
                var booking = new BookingDto
                {
                    Id = Guid.NewGuid().ToString(),
                    ChairNumber = bookedChair,
                    SessionId = session.Id,
                    ApplicationUserId = userId,
                    Status = "booked",
                    Date = date
                };
                _bookingService.Create(booking);
            }
            return Json("Your chairs are booked.", JsonRequestBehavior.AllowGet);
        }

        private void SetFilmsToSessions(IEnumerable<SessionViewModel> sessionsViewModel)
        {
            foreach (var session in sessionsViewModel)
            {
                var film = _filmService.Get(session.FilmId);
                film.Sessions = null;
                session.FilmViewModel = AutoMapperWeb.GetMapper().Map<FilmViewModel>(film);
            }
        }
    }
}