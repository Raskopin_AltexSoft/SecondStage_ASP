﻿using System.Drawing;
using System.Linq;
using System.Web;

namespace Cinema.WEB.Extensions
{
    public static class CheckPosterExtension
    {
        public static string CheckPoster(this HttpPostedFileBase poster)
        {
            const int width = 1200;
            const int height = 768;

            var allowedFileExtensions = new string[] { ".jpg" };
            if (poster == null)
                return "File is null";
            var image = Image.FromStream(poster.InputStream, true, true);

            if (!allowedFileExtensions.Contains(poster.FileName.Substring(poster.FileName.LastIndexOf('.'))))
                return "Please upload Your Photo of type: " + string.Join(", ", allowedFileExtensions);
            if (image.Width <= width || image.Height <= height)
                return "Upload image size must be minimum 1200x768";
            return null;
        }
    }
}