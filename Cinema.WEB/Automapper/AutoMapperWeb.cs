﻿using AutoMapper;
using Cinema.BLL.Automapper;
using Cinema.BLL.DTO;
using Cinema.WEB.ViewModels;

namespace Cinema.WEB.Automapper
{
    public static class AutoMapperWeb
    {
        private static readonly IMapper Mapper;
        static AutoMapperWeb()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<WebProfile>();
                cfg.AddProfile<BllProfile>();
            });
            Mapper = config.CreateMapper();
        }

        public static IMapper GetMapper()
        {
            return Mapper;
        }
    }
}