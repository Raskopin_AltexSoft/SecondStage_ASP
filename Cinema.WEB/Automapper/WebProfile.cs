﻿using AutoMapper;
using Cinema.BLL.DTO;
using Cinema.WEB.ViewModels;

namespace Cinema.WEB.Automapper
{
    public class WebProfile : Profile
    {
        public WebProfile()
        {
            CreateMap<FilmDto, FilmViewModel>().MaxDepth(1);
            CreateMap<FilmViewModel, FilmDto>();
            CreateMap<GenreDto, GenreViewModel>();
            CreateMap<GenreViewModel, GenreDto>();
            CreateMap<GenreCreateViewModel, GenreDto>();
            CreateMap<SessionDto, SessionViewModel>()
                .ForMember(s => s.FilmViewModel, opt => opt.MapFrom(src => src.FilmDto)).MaxDepth(1);
            CreateMap<ScheduleDto, ScheduleViewModel>()
                .ForMember(s => s.Sessions, opt => opt.MapFrom(src => src.Sessions));
        }
    }
}