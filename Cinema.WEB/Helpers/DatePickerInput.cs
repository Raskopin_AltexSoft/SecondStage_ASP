﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Cinema.WEB.Helpers
{
    public static class DatePickerInput
    {
        public static MvcHtmlString CreateDatePickerInput(this HtmlHelper html, ModelMetadata model = null)
        {
            var input = new TagBuilder("input");
            input.MergeAttribute("class", "datepicker");
            input.MergeAttribute("class", "form-control");
            input.MergeAttribute("class", "text-box");
            input.MergeAttribute("class", "single-line");
            input.MergeAttribute("data-val", "true");
            input.MergeAttribute("type", "date");
           // input.MergeAttribute("date-disabled", "true");
           // input.MergeAttribute("disabled", "");
            input.MergeAttribute("id", model.PropertyName);
            input.MergeAttribute("data-val-date", "The field " + model.ModelType.Name + " must be a date.");
            input.MergeAttribute("data-val-required", "The field " + model.ModelType.Name + " is requered.");
            //input.MergeAttribute("type", "text");
            //date-disabled="'true'" disabled
            input.MergeAttribute("name", model.PropertyName);
            if (model != null)
            {
                try
                {
                    var dt = DateTime.Parse(model.Model.ToString());
                    input.MergeAttribute("value", dt.ToString("yyyy-MM-dd"));
                    
                }
                catch (Exception)
                {
                    
                }
            }
            return new MvcHtmlString(input.ToString());
        }

        public static MvcHtmlString CreateDatePicker<TModel, TProperty>
            (this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return CreateDatePickerInput(htmlHelper, metadata);
        }
    }
}