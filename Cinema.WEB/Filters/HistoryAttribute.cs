﻿using System;
using System.Web.Mvc;
using Autofac;
using Cinema.BLL.Services;
using Cinema.WEB.DI;

namespace Cinema.WEB.Filters
{
    public class HistoryAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var ip = filterContext.RequestContext.HttpContext.Request.UserHostAddress;
            var browser = filterContext.RequestContext.HttpContext.Request.UserAgent;
            var date = DateTime.Now.Date;

            var historyService = AutofacConfig.Container.Resolve<HistoryService>();
            historyService.Create(ip, browser, date);
        }
    }
}